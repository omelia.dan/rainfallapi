﻿
namespace RainfallApi.Reponses
{
    public class ErrorResponse
    {
        public string Message { get; set; }
        public IEnumerable<ErrorDetail> Detail { get; set; }
    }
}
