﻿using Services.Models;

namespace RainfallApi.Reponses
{
    public class RainfallReadingResponse
    {
        public IEnumerable<RainfallReading> Readings { get; set; }
    }
}
