﻿using Microsoft.AspNetCore.Mvc;
using RainfallApi.Reponses;
using Services.Interface;
using System.ComponentModel.DataAnnotations;

namespace RainfallApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RainfallController : ControllerBase
    {
        private readonly IRainfallService _rainfallService;

        public RainfallController(IRainfallService rainfallService)
        {
            _rainfallService = rainfallService;
        }

        [HttpGet]
        [Route("{stationId:int}/readings")]
        [ProducesResponseType(typeof(RainfallReadingResponse), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        public async Task<ActionResult> GetStationReadings(int stationId, [FromQuery][Range(1, 100)] int count = 10)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new ErrorResponse { Message = "Invalid request", Detail = GetValidationErrors() });
                }

                var reading = await _rainfallService.GetRainfallReadings(stationId, count);

                if (reading is null || !reading.Any())
                {
                    return NotFound("No readings found for the specified stationId");
                }

                return Ok(new RainfallReadingResponse { Readings = reading });
            }
            catch (Exception ex)
            {
                // log here
                return StatusCode(500, CreateExceptionResponse(ex));
            }
        }

        private IEnumerable<ErrorDetail> GetValidationErrors()
        {
            return ModelState
                .Where(x => x.Value.Errors.Any())
                .Select(x => new ErrorDetail
                {
                    PropertyName = x.Key,
                    Message = x.Value.Errors.First().ErrorMessage
                });
        }

        private ErrorResponse CreateExceptionResponse(Exception ex)
        {
            return new ErrorResponse
            {
                Message = "Internal server error",
                Detail = new List<ErrorDetail>
                    {
                        new ErrorDetail
                        {
                            PropertyName = "InternalError",
                            Message = ex.Message
                        }
                    }
            };
        }
    }
}
