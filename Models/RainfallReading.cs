﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Models
{
    public class RainfallReading
    {
        [JsonPropertyName("dateTime")]
        public DateTime DateMeasured { get; set; }

        [JsonPropertyName("value")]
        public decimal AmountMeasured { get; set; }
    }
}
