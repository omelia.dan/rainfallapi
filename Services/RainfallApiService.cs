﻿using Microsoft.Extensions.Configuration;
using Services.Interface;
using Services.Models;
using System.Text.Json;

namespace Services
{
    public class RainfallApiService : IRainfallService
    {
        private string apiBaseUrl;
        private readonly HttpClient _httpClient;

        public RainfallApiService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            apiBaseUrl = configuration["ApiBaseUrl"];
        }

        public async Task<RainfallReading[]?> GetRainfallReadings(int stationId, int count)
        {
            var response = await _httpClient.GetAsync($"{apiBaseUrl}{stationId}/readings?_sorted&_limit={count}");
            var content = JsonSerializer.Deserialize<RainfallApiResponse>(await response.Content.ReadAsStringAsync());
            return content?.Items;
        }
    }
}
