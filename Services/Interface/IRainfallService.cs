﻿using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interface
{
    public interface IRainfallService
    {
        Task<RainfallReading[]?> GetRainfallReadings(int stationId, int count);
    }
}
