﻿using System.Text.Json.Serialization;

namespace Services.Models
{
    internal class RainfallApiResponse
    {
        [JsonPropertyName("items")]
        public RainfallReading[]? Items { get; set; }
    }
}
